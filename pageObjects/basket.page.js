const assert = require('assert');
class basket  {

    /* Mobile site page elements */

    get quantitySizeLabel()   { return browser.elements('.OrderProducts-label'); }
    get basketProductTitle() {return browser.element('.OrderProducts-productName'); }
    get deleteIcon()   {return browser.element('.OrderProducts-deleteIcon');  }
    get deleteButtonPopUp() {return browser.element('.Button.OrderProducts-deleteButton') }
    get closeIconPopUp() {return browser.element('.Button');}
    get basketEmptyMsg() {return browser.element('.MiniBag-emptyLabel')}

    /* Desktop site page elements */
    get desktopBasketProductTitle() {return browser.element('.item_title.product_lightbox'); }
    get desktopQuantitySizeLabel()   { return browser.element('.item_quantity'); }
    get desktopRemoveProductButton()   {return browser.element('.remove_bag_item');  }
    get desktopDeleteButtonPopUp() {return browser.element('.confirm_remove_from_bag.submit_button.chk_button_secondary') }
    get desktopBasketEmptyMsg() {return browser.element('#basket_empty')}


    /* page methods */

    // This method stores the title of the product in a global variable and returns the title from basket page. The first if statement will check if desktop site or mobile site.
    getBasketProductTitle() {

        if(this.desktopBasketProductTitle.isVisible()){
            this.desktopBasketProductTitle.waitForVisible(5000);
            global.desktopBasketTitle = this.desktopBasketProductTitle.getText();
            return this.desktopBasketProductTitle.getText();
        }
        else{
            this.basketProductTitle.waitForVisible(5000);
            global.basketTitle = this.basketProductTitle.getText();
            return this.basketProductTitle.getText();
        }

    }

    // This method return the quantity of the product from basket page.The first if statement will check if desktop site or mobile site.
    getBasketQuantity() {

        if(this.desktopBasketProductTitle.isVisible()){
            this.desktopQuantitySizeLabel.waitForVisible(5000);
            return(browser.elementIdText(this.desktopQuantitySizeLabel.value.ELEMENT).value).slice(-1);
        }
        else{
            this.quantitySizeLabel.waitForVisible(5000);
            return (browser.elementIdText(this.quantitySizeLabel.value[0].ELEMENT).value).slice(0,1);
        }

    }

    /*
    This method return the a boolean value based on comparing the product title fro Product Description page and Basket page which is stored in global variables.
    Before comparing the titles are converted to LowerCase as it was observed that some products are having case difference on PDP and Basket page (Eg. Small Flower And Tassel Earrings)
    The first if statement will check if desktop site or mobile site.
    */
    isProductTitleSame() {

        if(this.desktopBasketProductTitle.isVisible()){
            return assert.equal(global.desktopPdpTitle.toLowerCase(), global.desktopBasketTitle.toLowerCase());
        }
        else{
            return assert.equal(global.pdpTitle.toLowerCase(), global.basketTitle.toLowerCase());
        }

    }

    // This method compares the quantity of the product on basket page with the quantity selected on PDP (For now we have kept the quantity as static 3).
    isQuantitySame() {
        return assert.equal(3, this.getBasketQuantity());
    }

    // This method removes the product from the basket.The first if statement will check if desktop site or mobile site.
    removeProduct() {

        if(this.desktopRemoveProductButton.isVisible()){
            this.desktopRemoveProductButton.waitForVisible(5000);
            this.desktopRemoveProductButton.click();
            this.desktopDeleteButtonPopUp.waitForVisible(5000);
            this.desktopDeleteButtonPopUp.click();
        }
        else{
            this.deleteIcon.waitForVisible(5000);
            this.deleteIcon.click();
            this.deleteButtonPopUp.waitForVisible(5000);
            this.deleteButtonPopUp.click();
        }

    }

    // This method checks if teh basket is empty by comparing the message displayed. The first if statement will check if desktop site or mobile site.
    isBasketEmpty() {
        if(this.desktopBasketEmptyMsg.isVisible()){
            this.desktopBasketEmptyMsg.waitForVisible(5000);
            return assert.equal(this.desktopBasketEmptyMsg.getText(), "Your shopping bag is empty.")
        }
        else{
            this.basketEmptyMsg.waitForVisible(5000);
            return assert.equal(this.basketEmptyMsg.getText(), "Your shopping bag is currently empty.")
        }

    }
}

export default new basket();