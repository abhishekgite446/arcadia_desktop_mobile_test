import commonHelpers from './commonhelpers.page'
import homePage from './homepage.page'

class productlistingpage {

    /* Mobile site page elements */
    get resultTotal()   { return browser.element('.PlpHeader-total.PlpHeader-totalValue'); }
    get allProducts()   { return browser.elements('.Product.Product--col2');}

    /* Desktop site page elements */
    get desktopResultTotal()   { return browser.element('.count'); }
    get desktopAllProducts()   { return browser.elements('.product');}

    /* page methods */

    // This method will return the total results text displayed for a given product search. The if statement will check if it is desktop site or mobile site.
    totalResults() {
        browser.pause(2000);
        if(this.desktopResultTotal.isVisible()){
            this.desktopResultTotal.waitForVisible(5000);
            return this.desktopResultTotal.getText();
        }
        else
        {
            this.resultTotal.waitForVisible(5000);
            return this.resultTotal.getText();
        }
    }

    /*
    This method select a random product from the listing page. It initially will get an list for all the elements displayed
    then it will get random element from that list by using the getRandomNumber method. Then it will click on that object.
    It was rarely observed that a undefined element is returned when try to get a random element, in that case re-run the test.
    The if statement will check if it is desktop site or mobile site.
    */
    clickOnRandomProduct() {

        browser.pause(5000);

        if(homePage.desktopBrandLogo.isVisible()){
            var random = this.desktopAllProducts.value[commonHelpers.getRandomNumber(this.desktopAllProducts)]
            browser.pause(5000);
            browser.elementIdClick(random.ELEMENT);

        }
        else {
            var random = this.allProducts.value[commonHelpers.getRandomNumber(this.allProducts)]
            browser.pause(5000);
            browser.elementIdClick(random.ELEMENT);
        }
    }
}

export default new productlistingpage();