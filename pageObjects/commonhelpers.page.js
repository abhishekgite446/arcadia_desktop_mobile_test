const assert = require('assert');
class commonhelpers  {

    /* common methods */

    // This method check if the given element is visible or not
    elementIsVisible(locator) {
        locator.waitForVisible(5000);
        return assert.ok(locator.isVisible());
    }

    // This method returns random number between 1 and the max(Elements locator)
    getRandomNumber(max) {
        max.waitForVisible(5000);
        return Math.floor(Math.random() * (max.value.length - 1)) + 1;;
    }
}

export default new commonhelpers();