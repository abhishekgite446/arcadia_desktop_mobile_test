const assert = require('assert');
class productdescpage  {

    /* Mobile site page elements */
    get productDetailContainer()   { return browser.element('.ProductDetail'); }
    get quantityDropDown() { return '#productQuantity' }
    get allQuantities() { return browser.elements('.Select-select--option ') }
    get productTitle() {return browser.element('.ProductDetail-title'); }
    get addToBagButton() {return browser.element('.Button'); }
    get viewBagButtonPopUp() {return browser.element('.Button.AddToBagConfirm-viewBag.Button--secondary.Button--twoFifthWidth'); }
    get productSize() {return browser.elements('.ProductSizes-item'); }
    get productSizeList() {return browser.element('.ProductSizes-list') }

    /* Desktop site page elements */
    get desktopProductDetailContainer()   { return browser.element('#product-detail'); }
    get desktopProductSizeList() {return browser.element('.product_size_buttons') }
    get desktopQuantityDropDown() { return '.product_quantity' }
    get desktopAddToBagButton() {return browser.element('#btn_add_2_shop_cart'); }
    get desktopProductTitle() {return browser.element('.product_details.pull-right'); }
    get desktopProductTitleText() {return browser.element('h1'); }
    get desktopSizeHelper() {return browser.element('.size_helper'); }
    get desktopViewBagButtonPopUp() {return browser.element('#my_bag_icon'); }



    /* page methods */

    /*
    This method will select a quantity 3 (It can be chnaged). Additionally if a product is selected which needs size selection then a first in-stock size will be selected.
    The first if statement will check if it is desktop site or mobile site.
    */

    selectQuantity() {
        browser.pause(2000);

        if(this.desktopProductDetailContainer.isVisible()){
            // If the product needs size selection the following if statement will be executed
            if(this.desktopSizeHelper.isVisible()){
               browser.click('*//label[@class="btn"]');
            }
            browser.element(this.desktopQuantityDropDown).waitForVisible(5000);
            console.log("INSIDE");
            browser.selectByValue(this.desktopQuantityDropDown,3);
        }
        else {
            // If the product needs size selection the following if statement will be executed
            if(this.productSizeList.isVisible()){
                browser.click('*//span[@class="ProductSizes-item"]');
            }
            browser.element(this.quantityDropDown).waitForVisible(5000);
            browser.scroll(this.quantityDropDown);
            browser.selectByValue(this.quantityDropDown,3);
        }
    }

    // This method will adda product to the bag. The if statement will check if it is desktop site or mobile site.
    addtoBag() {
        if(this.desktopAddToBagButton.isVisible()){
            //browser.scroll(this.desktopAddToBagButton);
            this.desktopAddToBagButton.click();
        }
        else {
            this.addToBagButton.click();
        }
    }

    // This method will click on the 'View Bag' button from the pop-up. The if statement will check if it is desktop site or mobile site.
    viewBagPopUp() {
        if(this.desktopViewBagButtonPopUp.isVisible()){
            //this.desktopViewBagButtonPopUp.waitForVisible(5000);
            this.desktopViewBagButtonPopUp.click();
        }
        else{
            this.viewBagButtonPopUp.waitForVisible(5000);
            this.viewBagButtonPopUp.click();
        }
    }

    // This method will return the product title. The if statement will check if it is desktop site or mobile site.
    getProductTitle() {
        if(this.desktopProductTitle.isVisible()){
            global.desktopPdpTitle = this.desktopProductTitleText.getText();
            return this.desktopProductTitleText.getText();
        }
        else {
            global.pdpTitle = this.productTitle.getText();
            return this.productTitle.getText();
        }
    }

    // This method will return a boolean value if Product Detail container is visible or not. The if statement will check if it is desktop site or mobile site.
    hasProductDetailContainer() {
        if(this.desktopProductDetailContainer.isVisible()){
            return this.desktopProductDetailContainer.isVisible();
        }
        else{
            return this.productDetailContainer.isVisible();
        }
    }
}

export default new productdescpage();